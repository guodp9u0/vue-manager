import { param2Obj } from '@/utils'
import default_avatar from '@/assets/default_avatar.png';

const userMap = {
    admin: {
        roles: ['admin'],
        token: 'admin',
        introduction: '我是超级管理员',
        avatar: default_avatar,
        name: 'Super Admin'
    },
    editor: {
        roles: ['editor'],
        token: 'editor',
        introduction: '我是编辑',
        avatar: default_avatar,
        name: 'Normal Editor'
    }
}

export default {
    login: config => {
        const { username } = JSON.parse(config.body);
        return { code: 20000, data: userMap[username], msg: 'ok' };
    },
    getUserInfo: config => {
        const { token } = param2Obj(config.url);
        if (userMap[token]) {
            return { code: 20000, data: userMap[token], msg: 'ok' };
        } else {
            return {
                code: 20001,
                data: '',
                msg: '无效用户'
            };
        }
    },
    logout: () => { return { code: 20000, data: 'logout success', msg: 'ok' }; }
}