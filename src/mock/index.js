import Mock from 'mockjs'
import loginAPI from './user'

var mock = process.env.MOCK;

// Mock.setup({
//   timeout: '350-600'
// })

if (mock) {
    // 登录相关
    Mock.mock(/\/user\/login/, 'post', loginAPI.login)
    Mock.mock(/\/user\/logout/, 'post', loginAPI.logout)
    Mock.mock(/\/user\/info\.*/, 'get', loginAPI.getUserInfo)
}

export default Mock